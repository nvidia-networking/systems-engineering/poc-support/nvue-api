# NVUE API

This is a demo designed to introduce users on how to use the NVUE API.

The default password for the switches are:
```
Username: cumulus
Password: CumulusLinux!
```

The default password for the `oob-mgmt-server` is:
```
Username: ubuntu
Password: nvidia
```

## Create the service

First click the `+ Add Service` button:

![Add Service](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/Add%20Service.png)

Now add the service. We will be using TCP port 8765 as the NVUE API listening port.

![Define Service](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/API%20Service.png)

Once the service is added, it should show up with a public port.

![Service Added](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/Service%20Added.png)

This needs to be done for both devices in the topology `cumulus0` and `cumulus1`.

## Enabling NVUE API

The NVUE API is already enabled on both switches so no extra configuration is needed by the user. But, for posterity, the below commands were used to enable the API:

https://docs.nvidia.com/networking-ethernet-software/cumulus-linux-50/System-Configuration/NVIDIA-User-Experience-NVUE/#nvue-rest-api

```
cumulus@cumulus0:~$ sudo ln -s /etc/nginx/sites-{available,enabled}/nvue.conf
cumulus@cumulus0:~$ sudo sed -i 's/listen localhost:8765 ssl;/listen \[::\]:8765 ipv6only=off ssl;/g' /etc/nginx/sites-available/nvue.conf
cumulus@cumulus0:~$ sudo systemctl restart nginx
```

## Connecting to the API

### Using CLI

Verify that the API is active by using a simple CLI query via bash. This will use the `curl` command. The structure of the command:

```
curl -u 'cumulus:CumulusLinux!' --insecure https://<worker_ip_address>:<external_port>/cue_v1/interface
```

```
$ curl  -u 'cumulus:CumulusLinux!' --insecure https://worker09.air.nvidia.com:16090/cue_v1/interface
{
  "eth0": {
    "ip": {
      "address": {
        "192.168.200.2/24": {}
      }
    },
    ...
}
```

### Using Postman

To use Postman, ensure the following settings are present.

#### Disable SSL Verification

By default, Postman will do SSL verification. Turn this feature of `off`:

![SSL Verification](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/Postman%20SSH%20Verification.png)

#### Basic Auth

Change the authentication field to `basic auth` and configure the username/password as `cumulus/CumulusLinux!`:

![Basic Auth](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/Postman%20Basic%20Auth.png)


#### Use HTTPS for REST query

In the host field, put the full URL of the host:

![Host Field](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/nvue-api/-/raw/main/Postman%20Host%20Field.png)
